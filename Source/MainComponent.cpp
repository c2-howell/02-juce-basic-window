/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    audioDeviceManager.setMidiInputEnabled ("USB Axiom 49 Port 1", true);
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.initialiseWithDefaultDevices (2, 2);
    audioDeviceManager.addAudioCallback (this);
    gainSlider.setSliderStyle(Slider::Rotary);
    gainSlider.setRange(0, 1);
    gainSlider.addListener(this);
    gainSlider.setValue(0.75);
    addAndMakeVisible(&gainSlider);
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
    audioDeviceManager.removeAudioCallback (this);
}

void MainComponent::resized()
{
    gainSlider.setBounds(getWidth()/2, getHeight()/2, 40, 40);

}

void MainComponent::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    std::cout<<"MessageRecieved";
}

void MainComponent::audioDeviceAboutToStart (AudioIODevice* device)
{
    std::cout<<"audioIOStarted";
}

void MainComponent::audioDeviceStopped()
{
    std::cout<<"audioIOFinished";
}

void MainComponent::sliderValueChanged (Slider* slider)
{
    gain = gainSlider.getValue();
}

void MainComponent::audioDeviceIOCallback (const float
                                           ** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    while(numSamples--)
    {
        *outL = *inL*gain;
        *outR = *inR*gain;
        inL++;
        outL++;
        outR++;
    }
}